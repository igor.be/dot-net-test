﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TG.Exam.WebMVC.Models;

namespace TG.Exam.WebMVC.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var model = new HomeViewModel
            {
                Users = Models.User.GetAll()
            };

            return View(model);
        }

        public JsonResult Refresh()
        {
            var data = Models.User.GetAll();
            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}