﻿using System.Collections.Generic;

namespace TG.Exam.WebMVC.Models
{
    public class HomeViewModel
    {
        public List<User> Users { get; set; }
    }
}