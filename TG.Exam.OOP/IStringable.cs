﻿namespace Salestech.Exam.OOP
{
    interface IStringable
    {
        string ToString2();
    }

    // Contracts and class inheritance should follow domain logic for clarity of code and common sense reasons
    interface IStringableEmployee : IStringable
    {
    }
}
