﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Salestech.Exam.OOP;

namespace TG.Exam.OOP
{

    public class Employee : IStringable
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Salary { get; set; }
        
        public virtual string ToString2()
        {
            return $"E {LastName}, {FirstName} paid ${Salary}";
        }
    }

    public class SalesManager : Employee
    {
        public int BonusPerSale { get; set; }
        public int SalesThisMonth { get; set; }

        public override string ToString2()
        {
            // while it's tempting to do base.ToString2 + " " + ..., it makes code harder to read and debug.
            // inheritance level of 2+ is a good reason to consider replacing iheritance with decomposition
            return $"SM {LastName}, {FirstName} paid ${Salary} has bonus {BonusPerSale} ({SalesThisMonth} this month)";
        }
    }

    public class CustomerServiceAgent : Employee
    {
        public int Customers { get; set; }

        public override string ToString2()
        {
            return $"CSA {LastName}, {FirstName} paid ${Salary} handles {Customers})";
        }
    }

    public class Dog : IStringable
    {
        public string Name { get; set; }
        public int Age { get; set; }
        
        public string ToString2()
        {
            return $"D {Name}, {Age}";
        }
    }
}
