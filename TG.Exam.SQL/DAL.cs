﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TG.Exam.SQL
{
    public class DAL
    {
        private SqlConnection GetConnection() 
        {
            var connectionString = ConfigurationManager.AppSettings["ConnectionString"];

            var con = new SqlConnection(connectionString);

            con.Open();

            return con;
        }

        private DataSet GetData(string sql)
        {
            var ds = new DataSet();

            using (var con = GetConnection())
            {
                using (var cmd = new SqlCommand(sql, con))
                {
                    using (var adp = new SqlDataAdapter(cmd))
                    {
                        adp.Fill(ds);
                    }
                }
            }

            return ds;
        }

        private void Execute(string sql)
        {
            using (var con = GetConnection())
            {
                using (var cmd = new SqlCommand(sql, con))
                {
                    cmd.ExecuteNonQuery();
                }
            }
        }

        public DataTable GetAllOrders()
        {
            var sql = @"
SELECT [OrderId]
      ,[OrderCustomerId]
      ,[OrderDate]
FROM [dbo].[Orders]
";

            var ds = GetData(sql);

            var result = ds.Tables.OfType<DataTable>().FirstOrDefault();

            return result;
        }

        public DataTable GetAllOrdersWithCustomers()
        {
            var sql = @"
SELECT [OrderId]
      ,[OrderCustomerId]
      ,[OrderDate]
FROM [dbo].[Orders] AS O
WHERE EXISTS (
	SELECT 1 
	FROM [dbo].[Customers] AS C
	WHERE O.OrderCustomerId = C.CustomerId)
";
            var ds = GetData(sql);

            var result = ds.Tables.OfType<DataTable>().FirstOrDefault();

            return result;
        }

        public DataTable GetAllOrdersWithPriceUnder(int price)
        {
            var sql = @"
SELECT O.OrderId
      ,O.OrderCustomerId
      ,O.OrderDate
	  ,sums.TotalPrice
FROM [dbo].[Orders] AS O
JOIN
(SELECT OI.OrderId, SUM(I.ItemPrice) AS TotalPrice
  FROM [dbo].[OrdersItems] AS OI 
  JOIN  [dbo].[Items] AS I ON I.ItemId = OI.ItemId
  GROUP BY OI.OrderId
) AS sums
ON O.OrderId = sums.OrderId
WHERE sums.TotalPrice < {0}
";

            sql = string.Format(sql, price);
            var ds = GetData(sql);

            var result = ds.Tables.OfType<DataTable>().FirstOrDefault();

            return result;
        }

        public void DeleteCustomer(int orderId)
        {
            var sql = @"
DELETE C FROM [dbo].[Customers] AS C
WHERE C.CustomerId IN (
	SELECT O.OrderCustomerId 
	FROM [dbo].[Orders] O 
	WHERE O.OrderId = {0}
)
";

            sql = string.Format(sql, orderId);

            Execute(sql);
        }

        internal DataTable GetAllItemsAndTheirOrdersCountIncludingTheItemsWithoutOrders()
        {
            var sql = @"
SELECT I.[ItemId]
      ,I.[ItemName]
      ,I.[ItemPrice]
	  ,ISNULL(ItemCounts.OrderCount, 0) AS OrderCount
FROM [dbo].[Items] AS I
LEFT JOIN
(SELECT COUNT(OI.[OrderId]) AS OrderCount
      ,OI.ItemId
FROM [dbo].[OrdersItems] AS OI
GROUP BY OI.ItemId) AS ItemCounts
ON ItemCounts.ItemId = I.ItemId
";

            var ds = GetData(sql);

            var result = ds.Tables.OfType<DataTable>().FirstOrDefault();

            return result;
        }
    }
}
