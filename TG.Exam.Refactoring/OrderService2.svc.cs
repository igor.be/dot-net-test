﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using log4net;

namespace TG.Exam.Refactoring
{
    public class OrderService2 : IOrderService
    {
        private readonly ILog _logger;

        private readonly string _connectionString;

        public IDictionary<string, Order> Cache { get; } = new Dictionary<string, Order>();

        private readonly object _lockRoot = new object();

        public OrderService2()
        {
            _logger = LogManager.GetLogger(typeof(OrderService));
            _connectionString = ConfigurationManager.ConnectionStrings["OrdersDBConnectionString"].ConnectionString;
        }

        public Order LoadOrder(string orderId)
        {
            Stopwatch stopWatch = new Stopwatch();
            stopWatch.Start();
            try
            {
                return LoadOrderInner(orderId);
            }
            catch (SqlException ex)
            {
                _logger.Error(ex.Message);
                throw new ApplicationException("Error", ex);
            }
            finally
            {
                _logger.InfoFormat("Elapsed - {0}", stopWatch.Elapsed);
            }
        }

        private Order LoadOrderInner(string orderId)
        {
            if (string.IsNullOrEmpty(orderId))
                throw new ArgumentException("Required", nameof(orderId));

            if (Cache.TryGetValue(orderId, out var result))
                return result;

            lock (_lockRoot)
            {
                if (Cache.TryGetValue(orderId, out result))
                    return result;

                return TryGetFromDb(orderId);
            }
        }

        private Order TryGetFromDb(string orderId)
        {
            using (SqlConnection connection = new SqlConnection(this._connectionString))
            {
                connection.Open();
                string queryTemplate =
                    "SELECT TOP 1 OrderId, OrderCustomerId, OrderDate" +
                    "  FROM dbo.Orders WHERE OrderId=@Id";
                using (SqlCommand command = new SqlCommand(queryTemplate, connection))
                {
                    command.Parameters.AddWithValue("@Id", orderId);
                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        if (!reader.Read())
                            return null;

                        Order order = new Order
                        {
                            OrderId = (int) reader[0],
                            OrderCustomerId = (int) reader[1],
                            OrderDate = (DateTime) reader[2]
                        };

                        Cache[orderId] = order;

                        return order;
                    }
                }
            }
        }
    }
}
