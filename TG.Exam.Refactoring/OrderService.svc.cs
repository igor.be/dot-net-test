﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Diagnostics;
using log4net;
using log4net.Config;

namespace TG.Exam.Refactoring
{
    public class OrderService : IOrderService
    {
        // maybe better to pass as injected implementation for cleaner tests
        private static readonly ILog logger = LogManager.GetLogger(typeof(OrderService));

        // pass connection string only to decouple from ConfigurationManager
        readonly string connectionString = ConfigurationManager.ConnectionStrings["OrdersDBConnectionString"].ConnectionString;

        // make property
        public IDictionary<string, Order> cache = new Dictionary<string, Order>();

        public OrderService()
        {
            BasicConfigurator.Configure();
        }

        public Order LoadOrder(string orderId)
        {
            try
            {
                // use String.IsNullOrEmpty, don't use null-first notation
                Debug.Assert(null != orderId && orderId != "");
                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();
                // lock on public settable field
                // do check-lock-check instead or use some synchronized collection
                
                lock (cache)
                {
                    if (cache.ContainsKey(orderId))
                    {
                        stopWatch.Stop();
                        // move to finally
                        logger.InfoFormat("Elapsed - {0}", stopWatch.Elapsed);
                        return cache[orderId];
                    }
                }

                // use SQL parameters
                string queryTemplate =
                  "SELECT OrderId, OrderCustomerId, OrderDate" +
                  "  FROM dbo.Orders where OrderId='{0}'";
                string query = string.Format(queryTemplate, orderId);
                // using missing
                SqlConnection connection =
                  new SqlConnection(this.connectionString);
                // using missing
                SqlCommand command =
                  new SqlCommand(query, connection);
                connection.Open();
                // using missing
                SqlDataReader reader = command.ExecuteReader();
                if (reader.Read())
                {
                    Order order = new Order
                    {
                        OrderId = (int) reader[0],
                        OrderCustomerId = (int) reader[1],
                        OrderDate = (DateTime) reader[2]
                    };

                    // either lock everything to avoid going to dabase at all, or don't lock at all on set
                    lock (cache)
                    {
                        if (!cache.ContainsKey(orderId))
                            cache[orderId] = order;
                    }
                    stopWatch.Stop();
                    logger.InfoFormat("Elapsed - {0}", stopWatch.Elapsed);
                    return order;
                }
                stopWatch.Stop();
                logger.InfoFormat("Elapsed - {0}", stopWatch.Elapsed);
                return null;
            }
            catch (SqlException ex)
            {
                logger.Error(ex.Message);
                // lost stack trace here
                throw new ApplicationException("Error");
            }
        }
    }
}
