﻿using System;
using System.Linq;

namespace TG.Exam.Algorithms
{
    class Program
    {
        static int Foo(int a, int b, int c)
        {
            if (1 < c)
                return Foo(b, b + a, c - 1);
            else
                return a;
        }

        static int FooFibonacci(int first, int second, int number)
        {
            // recursion better be avoided for performance reasons
            // in CLR as stack overflow is harder to get, still a possibility, and stack pop/push is a heavier operation
            while (number > 1)
            {
                var sum = first + second;
                first = second;
                second = sum;
                number -= 1;
            }

            return first;
        }

        static int[] Bar(int[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
                for (int j = 0; j < arr.Length - 1; j++)
                    if (arr[j] > arr[j + 1])
                    {
                        int t = arr[j];
                        arr[j] = arr[j + 1];
                        arr[j + 1] = t;
                    }
            return arr;
        }

        // one of least effective sort alogrithms
        static int[] BarBubbleSort(int[] arr)
        {
            for (int i = 0; i < arr.Length; i++)
            {
                // flag for earlier sorting finished
                bool hadSwaps = false;
                // i less iterations here
                for (int j = 0; j < arr.Length - 1 - i; j++)
                {
                    var current = arr[j];
                    var next = arr[j + 1];
                    if (current > next)
                    {
                        hadSwaps = true;
                        int t = current;
                        arr[j] = next;
                        arr[j + 1] = t;
                    }
                }
                if (!hadSwaps)
                    break;
            }

            return arr;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Foo result: {0}", Foo(7, 2, 8));
            Console.WriteLine("Bar result: {0}", string.Join(", ", Bar(new[] { 7, 2, 8 })));
            
            var ranged1 = Enumerable.Range(1, 10).Select(i => (object)Foo(0, 1, i)).ToArray();
            var ranged2 = Enumerable.Range(1, 10).Select(i => (object)FooFibonacci(0, 1, i)).ToArray();
            Console.WriteLine("Foo result mult.: {0}", string.Join(", ", ranged1));
            Console.WriteLine("My result mult.: {0}", string.Join(", ", ranged2));
            
            var arr = new[] { 101, 7, 2, 8, 100, 1, 20 };
            Console.WriteLine("Bar result 2: {0}", string.Join(", ", Bar(arr)));
            Console.WriteLine("My  result 2: {0}", string.Join(", ", BarBubbleSort(arr)));
            Console.WriteLine("Std result 2: {0}", string.Join(", ", arr.OrderBy(i => i).ToArray()));

            Console.ReadKey();
        }
    }
}
